package com.dianakisil.iplatotest

import android.app.Application
import com.dianakisil.iplatotest.di.ComponentsHolder

class App: Application()
{
    lateinit var componentHolder: ComponentsHolder

    companion object
    {
        lateinit var instance: App
            private set
    }

    override fun onCreate()
    {
        super.onCreate()
        instance = this@App
        componentHolder = ComponentsHolder(this).init()
    }
}