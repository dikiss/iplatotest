package com.dianakisil.iplatotest.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dianakisil.iplatotest.data.repo.Repo
import com.dianakisil.iplatotest.utils.DataMapper
import com.dianakisil.iplatotest.utils.Validator

class LoginFactory(private val repo: Repo, private val validator: Validator, private val mapper: DataMapper): ViewModelProvider.Factory
{
    override fun <T: ViewModel> create(modelClass: Class<T>): T
    {
        return modelClass.getConstructor(
            Repo::class.java,
            Validator::class.java,
            DataMapper::class.java)
            .newInstance(repo, validator, mapper)
    }
}


