package com.dianakisil.iplatotest.data.repo

import com.dianakisil.iplatotest.data.remote.LoginRequest
import com.dianakisil.iplatotest.data.remote.LoginResponse
import com.dianakisil.iplatotest.data.remote.LoginRestController

class Repo(private val restController: LoginRestController)
{
    fun requestData(loginRequest: LoginRequest): LoginResponse
    {
        return restController.getUser(loginRequest)
    }
}