package com.dianakisil.iplatotest.utils

import com.dianakisil.iplatotest.data.remote.LoginRequest

class Validator
{
    // It is just a simple sample in real app you need to provide here all requested
    // validating logic
    fun isValid(loginRequest: LoginRequest): Boolean
            = loginRequest.username.isNotEmpty() && loginRequest.password.isNotEmpty()
}
