package com.dianakisil.iplatotest.utils

import com.dianakisil.iplatotest.data.model.User
import com.dianakisil.iplatotest.data.remote.LoginResponse

class DataMapper
{
    // It is just a sample in real app here you have to provide all converting logic
    fun convert(loginResponse: LoginResponse): User = loginResponse.user
}