package com.dianakisil.iplatotest.utils

enum class LoadingStatus
{
    SUCCESS, ERROR, LOADING
}