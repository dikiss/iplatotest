package com.dianakisil.iplatotest.di.components;

import com.dianakisil.iplatotest.di.ComponentsHolder;
import com.dianakisil.iplatotest.di.moduls.AppModule;
import com.dianakisil.iplatotest.di.scopes.AppScope;

import dagger.Component;

@AppScope
@Component(modules = AppModule.class)
public interface AppComponent
{
    void injectComponentsHolder(ComponentsHolder componentsHolder);
}