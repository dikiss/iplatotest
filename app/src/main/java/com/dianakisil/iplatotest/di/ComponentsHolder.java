package com.dianakisil.iplatotest.di;

import android.content.Context;

import com.dianakisil.iplatotest.di.components.AppComponent;
import com.dianakisil.iplatotest.di.components.BaseComponent;
import com.dianakisil.iplatotest.di.components.BaseComponentBuilder;
import com.dianakisil.iplatotest.di.moduls.AppModule;
import com.dianakisil.iplatotest.di.moduls.BaseModule;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

public class ComponentsHolder
{
    private final Context context;

    @Inject
    Map<Class<?>, Provider<BaseComponentBuilder>> builders;

    private Map<Class<?>, BaseComponent> components;
    private AppComponent appComponent;

    public ComponentsHolder(Context context)
    {
        this.context = context;
    }

    public ComponentsHolder init()
    {
        appComponent = DaggerAppCompoent.builder().appModule(new AppModule(context)).build();
        appComponent.injectComponentsHolder(this);
        components = new HashMap<>();
        return this;
    }

    public BaseComponent getBaseComponent(Class<?> cls)
    {
        return getBaseComponent(cls, null);
    }

    public BaseComponent getBaseComponent(Class<?> cls, BaseModule module)
    {
        BaseComponent component = components.get(cls);

        if (component == null)
        {
            BaseComponentBuilder builder = builders.get(cls).get();

            if (module != null)
            {
                builder.module(module);
            }

            component = builder.build();
            components.put(cls, component);
        }

        return component;
    }

    public void releaseBaseComponent(Class<?> cls)
    {
        components.put(cls, null);
    }
}
