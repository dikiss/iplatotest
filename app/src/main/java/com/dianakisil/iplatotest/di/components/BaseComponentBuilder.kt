package com.dianakisil.iplatotest.di.components

import com.dianakisil.iplatotest.di.moduls.BaseModule

interface BaseComponentBuilder<C : BaseComponent<*>?, M : BaseModule?> {
    fun build(): C
    fun module(module: M): BaseComponentBuilder<C, M>?
}