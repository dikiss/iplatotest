package com.dianakisil.iplatotest.di.moduls;


import com.dianakisil.iplatotest.data.remote.LoginRestController;
import com.dianakisil.iplatotest.data.repo.Repo;
import com.dianakisil.iplatotest.ui.main.LoginFactory;
import com.dianakisil.iplatotest.utils.DataMapper;
import com.dianakisil.iplatotest.utils.Validator;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginActModule implements BaseModule
{
    @Provides
    LoginFactory getFactory(Repo iRepo, Validator iValidator, DataMapper iMapper)
    {
        return new LoginFactory(iRepo, iValidator, iMapper);
    }

    @Provides
    Repo getRepo(LoginRestController iController)
    {
        return new Repo(iController);
    }

    @Provides
    LoginRestController getController()
    {
        return new LoginRestController();
    }

    @Provides
    Validator getValidator()
    {
        return new Validator();
    }

    @Provides
    DataMapper getMapper()
    {
        return new DataMapper();
    }
}