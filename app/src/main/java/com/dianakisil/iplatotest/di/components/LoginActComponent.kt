package com.dianakisil.iplatotest.di.components

import com.dianakisil.iplatotest.di.moduls.LoginActModule
import com.dianakisil.iplatotest.di.scopes.LoginActScope
import com.dianakisil.iplatotest.ui.main.LoginActivity
import dagger.Subcomponent


@LoginActScope
@Subcomponent(modules = [LoginActModule::class])
interface LoginActComponent : BaseComponent<LoginActivity?> {
    @Subcomponent.Builder
    interface Builder : BaseComponentBuilder<LoginActComponent?, LoginActModule?>
}