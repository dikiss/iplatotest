package com.dianakisil.iplatotest.di.moduls

import android.content.Context
import com.dianakisil.iplatotest.di.components.BaseComponentBuilder
import com.dianakisil.iplatotest.di.components.LoginActComponent
import com.dianakisil.iplatotest.di.scopes.AppScope
import com.dianakisil.iplatotest.ui.main.LoginActivity
import dagger.Module
import dagger.Provides
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap


@Module(
    subcomponents = [ //
        LoginActComponent::class]
)
class AppModule(context: Context) {
    private val context: Context
    @AppScope
    @Provides
    fun provideContext(): Context {
        return context
    }

    @Provides
    @IntoMap
    @ClassKey(LoginActivity::class)
    fun provideTimelineOfflineFragmentBuilder(builder: LoginActComponent.Builder): BaseComponentBuilder<*, *> {
        return builder
    }

    init {
        this.context = context
    }
}