package com.dianakisil.iplatotest.di.components

interface BaseComponent<A> {
    fun inject(activity: A)
}